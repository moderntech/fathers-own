<?php
session_start();
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', function() {
	return View::make('index');
});

Route::get('/category', function() {
	return View::make('category');
});

Route::get('/item', function() {
	return View::make('item');
});

Route::get('dofblogin', function() {
	try {
		$facebook = new FathersOwn\Facebook(url('login/facebook/callback'));
		$fb_login_url = $facebook->getLoginURL();
		return Redirect::away($fb_login_url);
	}
	catch (FacebookRequestException $ex) {
		return Redirect::away('/test')->with('message', $ex->getMessage());
	}
	catch (Exception $ex) {
		return Redirect::away('/test')->with('message', $ex->getMessage());
	}
});

Route::get('login/facebook/callback', function() {
	try {
		$facebook = new FathersOwn\Facebook(url('login/facebook/callback'));
		$profile = $facebook->getUserProfile();
		$firstName =  $profile['first_name'];
		$lastName = $profile['last_name'];
		return View::make('/test')->with([
			'username' => $firstName . ' ' . $lastName,
			'picture' => 'https://graph.facebook.com/'. $profile['id'] . '/picture?width=30&height=30'
		]);
		
	}
	catch (FacebookRequestException $ex) {
		return Redirect::away('/test')->with('message', $ex->getMessage());
	}
	catch (Exception $ex) {
		return Redirect::away('/test')->with('message', $ex->getMessage());
	}	
});

Route::get('test', function() {
	return View::make('test');
});
