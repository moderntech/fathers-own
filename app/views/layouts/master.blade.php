<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>FathersOwn | @section('pageTitle') @show</title>
    <link href='https://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ secure_asset('stylesheets/app.css') }}" />
    <script src="{{ secure_asset('js/vendor/modernizr.js') }}"></script>
    
    <style>

    </style>
  </head>
  <body>
    
    @section('content')
    @show
    
    <script src="{{ secure_asset('js/vendor/jquery.js') }}"></script>
    <script src="{{ secure_asset('js/foundation.min.js') }}"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>

