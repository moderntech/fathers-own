@extends('layouts.master')

@section('pageTitle')
Test Page | Foundation
@stop

@section('content')
<div class="row">
    <div class="sticky">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name">
              <h1><a href="#">Father's Own</a></h1>
            </li>
             <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
          </ul>
        
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
              <li class="active has-dropdown">
                  <a href="#">
                      @if (isset($picture))
                          <img src='{{ $picture }}' style='margin-right: 5px; position: relative; top: -2px; border: solid black 1px; border-radius: 3000px' />
                      @endif
                      {{ $username or 'Login' }}
                  </a>
                  <ul class='dropdown'>
                      @if (!isset($username))
                          <li><a href='{{ url('/dofblogin') }}'>Login with Facebook</a></li>
                      @else
                          <li><a href='#'>Manage Account</a></li>
                          <li><a href='#'>Log Out</a></li>
                      @endif
                  </ul>
              </li>
              <li class="has-dropdown">
                <a href="#">Browse</a>
                <ul class="dropdown">
                  <li><a href='#'>by Category</a></li>
                  <li><a href='#'>by Price</a></li>
                  <li><a href='#'>by Popularity</a></li>
                  <li></li>
                </ul>
              </li>
            </ul>
        </nav>
    </div>
    <div class = "row">
        <div class='medium-3 columns'>
          <br />
          <ul class="no-bullet">
            <li><a href='#'>Category</a></li>
            <li><a href='#'>Another Category</a></li>
            <li><a href='#'>One with Subcategories
              <ul>
                <li>Subcategory</li>
                <li>Another One</li>
              </ul>
            </a></li>
            <li><a href='#'>Yet Another Category</a></li>
          </ul>
      	</div>
        <div class='medium-9 columns'>
          <br />
          <ul class="small-block-grid-2 medium-block-grid-3">
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
            <li><img src='http://placehold.it/200x200/' /></li>
          </ul>
        </div>
    </div>
    <div class="row">
      <div class="small-12 columns">
       Copyright &copy;2014 Father's Own - All Rights Reserved
      </div>
    </div>
</div>              

</div>
@stop
