@extends('layouts.master')

@section('pageTitle')
Test Page | Foundation
@stop

@section('content')
<div class="row">
    <div class="sticky">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name">
              <h1><a href="#">Father's Own</a></h1>
            </li>
             <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
          </ul>
        
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
              <li class="active has-dropdown">
                  <a href="#">
                      @if (isset($picture))
                          <img src='{{ $picture }}' style='margin-right: 5px; position: relative; top: -2px; border: solid black 1px; border-radius: 3000px' />
                      @endif
                      {{ $username or 'Login' }}
                  </a>
                  <ul class='dropdown'>
                      @if (!isset($username))
                          <li><a href='{{ url('/dofblogin') }}'>Login with Facebook</a></li>
                      @else
                          <li><a href='#'>Manage Account</a></li>
                          <li><a href='#'>Log Out</a></li>
                      @endif
                  </ul>
              </li>
              <li class="has-dropdown">
                <a href="#">Browse</a>
                <ul class="dropdown">
                  <li><a href='#'>by Category</a></li>
                  <li><a href='#'>by Price</a></li>
                  <li><a href='#'>by Popularity</a></li>
                  <li></li>
                </ul>
              </li>
            </ul>
        </nav>
    </div>
    <div class = "row">
        <div class='medium-8 columns'>
          <img src="http://placehold.it/1000x1000/" />
          <style>
            #smallpics {
              margin-top: 5px;
            }
            #smallpics li {
              padding-left: 3px;
              padding-right: 3px;
            }
          </style>
          <ul id='smallpics' class="small-block-grid-3">
            <li><img src='http://placehold.it/300x200/' /></li>
            <li><img src='http://placehold.it/300x200/' /></li>
            <li><img src='http://placehold.it/300x200/' /></li>
          </ul>
        </div>
        <div class='medium-4 columns'>
          <h3>Item Title</h3>
          <h2>$0.00</h2>          
          <h5>Item short description here.</h5>
          <hr />
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <hr />
          <p>Materials: XXXXX</p>
          <p>Other Info: XXXX </p>
        </div>        
    </div>
    <div class="row">
      <div class="small-12 columns">
       Copyright &copy;2014 Father's Own - All Rights Reserved
      </div>

    </div>
</div>              

</div>
@stop
