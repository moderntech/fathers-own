<?php
// social.php
// configure values for social media APIs

return [
	'facebook' => [
		'appId'  => $_ENV['FB_APP_ID'],
		'secret' => $_ENV['FB_APP_SECRET'],
	],
];